#Changelog

##v1.0.0

* First version.

##v1.1.0

* New Bluetooth Condition
* New Start and Stop sdk methods.
* Small bug fixes.