#Installation

The easiest way to integrate Noteacons in your project is using _Carthage_ or _CocoaPods_:

###Carthage
```
github "Deveryware/noteacons-ios"
```

###CocoaPods
```
pod 'NoteaconsSDK'
```

###Download from the dashboard
If you don't want to use _Carthage_ or _CocoaPods_, you can download the SDK from the Dashboard and install following the next steps.

Follow these steps to integrate the Noteacons SDK within an existing project:

To add the SDK in Xcode:

1. Open your application's Xcode project.
2. If you don't have a **Frameworks** group in your project, create one.
3. Drag the **NoteaconsSDK.framework** into the **Frameworks** group of Xcode's Project Navigator.
4. Select **Copy items into destination group's folder**.
![Drag the Framework1](img/installation_import_framework.png)
![Drag the Framework2](img/installation_link_binaries.png)
5. Include the **NoteaconsSDK.framework** in **Embed Frameworks** section.
![Embed the Framework](img/installation_embed_framework.png)
