#Customization

##Getting the latest data

Use the refreshData method to update the data of the SDK.

```

- (void)refreshDataOnSuccess:(NTCSuccessBlock)success onFailure:(NTCFailureBlock)failure;

```

##Device Identifier

You can get our device identifier calling the `getDeviceId` method. The best place to call the `getDeviceId` method is inside the `onReady` callback:

```
- (void)onReady
{
    NSInteger deviceId = [[Noteacons manager] getDeviceId];
}
```

##Delegates

We provide five protocols that you can implement. `NTCInitializationDelegate`, `NTCBeaconEventDelegate`, `NTCGeoFenceEventDelegate`, `NTCCampaignEventDelegate` and `NTCActionEventDelegate`.

###NTCInitializationDelegate

Implement the `NTCInitializationDelegate` to get notified when the Noteacons SDK is ready to use:

```
- (void)onReady
{
    //Method called when the initialization of the SDK has finished.
}
```

Note that you must set the delegate:

```
[Noteacons manager].initializationDelegate = YOUR_DELEGATE;
```

###NTCBeaconEventDelegate

Implement the `NTCBeaconEventDelegate` to get notified when the device enters or leaves the range of a beacon:

```
- (void)onEnterBeacon:(NTCBeaconResource *)beacon
{
    NSLog(@"onEnterBeacon");
}

- (void)onExitBeacon:(NTCBeaconResource *)beacon
{
    NSLog(@"onExitBeacon");
}
```

Note that you must set the delegate:

```
[Noteacons manager].beaconEventDelegate = YOUR_DELEGATE;
```

###NTCGeoFenceEventDelegate

Implement the `NTCGeoFenceEventDelegate` to get notified when the device enters or leaves the range of a geofence:

```
- (void)onEnterGeoFence:(NTCGeofenceResource *)geofence
{
    NSLog(@"onEnterGeoFence");
}

- (void)onExitGeoFence:(NTCGeofenceResource *)geofence
{
    NSLog(@"onExitGeoFence");
}
```

Note that you must set the delegate:

```
[Noteacons manager].geoFenceEventDelegate = YOUR_DELEGATE;
```

###NTCCampaignEventDelegate

Implement the `NTCCampaignEventDelegate` to get notified before and after a campaign is launched:

```
- (BOOL)willLaunchCampaign:(NTCContextResource *)context
{
    NSLog(@"willLaunchCampaign");
    return YES;
}

- (void)didLaunchCampaign:(NTCContextResource *)context
{
    NSLog(@"didLaunchCampaign");
}
```

If you return false in the method `willLaunchCampaign:(NTCContextResource *)context` the campaign won't be launched.

Note that you must set the delegate:

```
[Noteacons manager].campaignEventDelegate = YOUR_DELEGATE;
```

###NTCActionEventDelegate

Implementing the ActionNotifier, you can override the default behaviour of the SDK.


```
-(BOOL) onExecuteCustomCallbackAction:(NTCBaseActionResource *)action withFunctionName:(NSString *)functionName withParams:(NSString *)params
{
    //Do something
    return NO; 
}

- (BOOL) onExecuteMessageAction:(NTCBaseActionResource *)action withTitle:(NSString *)title withMessage:(NSString *)message
{
    //Default behaviour
    return YES;
}
```

Note that you must set the delegate:

```
[Noteacons manager].actionEventDelegate = YOUR_DELEGATE;
```

If you want to provide a different implementation of the Action Message, you can override the `onExecuteMessageAction`: 

```
- (BOOL) onExecuteMessageAction:(NTCBaseActionResource *)action withTitle:(NSString *)title withMessage:(NSString *)message
{
    // Here do your implementation of Action Message.
    // When the action is finished, you need to call [action finish];
    // Return NO to NOT run the default implementation.
    return NO;
}
```

####Custom Callback

To implement the custom callback, you have to override the `onExecuteCustomCallbackAction`:

```
-(BOOL) onExecuteCustomCallbackAction:(NTCBaseActionResource *)action withFunctionName:(NSString *)functionName withParams:(NSString *)params
{
    // Do what you want here.
    // When the action is finished, you must call [action finish].
    // Return NO to NOT run the default implementation
    return NO; 
}
```








