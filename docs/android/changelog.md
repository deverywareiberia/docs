#Changelog

##v1.0.0

* First version.

##v1.1.0

* New Bluetooth Condition.
* New start & stop SDK methods.

##v1.1.1

* Stop monitoring geofences in StopSDK method.

##v1.1.2

* Avoid null pointer exception when receiving a null location.

##v1.1.3

* Check if GoogleApiClient is connected before stopping location updates.

##v1.1.4

* Allow action message customization