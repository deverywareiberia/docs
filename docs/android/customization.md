#Customization

##Listening for events

If you don't want to have the SDK always running in the background, you can manage when to stop the SDK functionalities and resume them whenever you prefer.

Once you call the `stopSDK()` method the SDK will stop tracking visits and launching campaigns. You can then call the `startSDK()` if you want to resume to normal usage.

##Listening for beacons in the background

In order to listen for beacons in the background, the SDK wakes up your app when is closed from the recent task list. If you don't want the app to be restarted, you can set the service_enabled meta-data to false:

```
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.deveryware.noteaconsdemo">

    <application
        ...
        <meta-data android:name="com.deveryware.noteacons.SERVICE_ENABLED" android:value="false"/>
        ...
    </application>

</manifest>
```


##Getting the latest data

The `refreshData(NoteaconNetworkCallback noteaconNetworkCallback)` method will update the data of the SDK.

##Device Identifier

You can get our device identifier calling the `getDeviceId()` method. The best place to call the `getDeviceId()` method is inside the `onNoteaconReady()` callback:

```
public class MyApplication extends Application implements NoteaconsNotifier {
    private static final String TAG = "MyApplication";

    @Override
    public void onCreate() {
        super.onCreate();
        Noteacons.initSDK(this);
        Noteacons.setNoteaconNotifier(this);
    }

    @Override
    public void onNoteaconsReady() {
        Log.i(TAG, "Ready, device id: "+ Noteacons.getDeviceId());
    }
}
```

##Setting the log level

The log level can be set via manifest.xml file:

```
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.deveryware.noteaconsdemo">

    <application
        ...
        <meta-data android:name="com.deveryware.noteacons.LOG_LEVEL" android:value="info"/>
        ...
    </application>

</manifest>
```

There are six possible levels: "none", "verbose", "debug", "info", "warning" and "error".

##Interfaces

We provide four interfaces that you can implement. `BeaconNotifier`, `GeofenceNotifier`, `CampaignNotifier` and `ActionNotifier`.

###BeaconNotifier

Implement the `BeaconNotifier` to get notified when the device enters or leaves the range of a beacon:

```
public class MyBeaconNotifier implements BeaconNotifier {

    private static final String TAG = "MyBeaconNotifier";

    @Override
    public void onEnterBeacon(NoteaconsBeacon beacon) {
        Log.d(TAG, beacon.toString());
    }

    @Override
    public void onExitBeacon(NoteaconsBeacon beacon) {
        Log.d(TAG, beacon.toString());
    }
}
```

Note that you must call the `setBeaconNotifier(BeaconNotifier beaconNotifier)` method:

```
Noteacons.setBeaconNotifier(new MyBeaconNotifier());
```

###CampaignNotifier

Implement the `CampaignNotifier` to get notified before and after a campaign is launched:

```
public class MyCampaignNotifier implements CampaignNotifier {

    private static final String TAG = "MyCampaignNotifier";

    @Override
    public boolean willLaunchCampaign(NoteaconsContext context) {
        Log.d(TAG, "willLaunchCampaign");
        return  true;
    }

    @Override
    public void didLaunchCampaign(NoteaconsContext context) {
        Log.d(TAG, "didLaunchCampaign");
    }
}
```

If you return false in the method `willLaunchCampaign(NoteaconsContext context)` the campaign won't be launched.

Note that you must call the `setCampaignNotifier(CampaignNotifier campaignNotifier)` method:

```
Noteacons.setCampaignNotifier(new MyCampaignNotifier());
```

###ActionNotifier

Implementing the ActionNotifier, you can override the default behaviour of the SDK.


```
public class MyActionNotifier implements ActionNotifier {

    @Override
    public boolean onExecuteActionCustomCallback(NoteaconsAction action, String functionName, String params) {
        return true;
    }

    @Override
    public boolean onExecuteActionMessage(NoteaconsAction action, String title, String message) {
        //Return false to use the default implementation.
        return false;
    }
}
```

Note that you must call the `setActionNotifier(ActionNotifier actionNotifier)` method:

```
Noteacons.setActionNotifier(new MyActionNotifier());
```


If you want to provide a different implementation of the Action Message, you can override the `onExecuteActionMessage`: 

```
@Override
    public boolean onExecuteActionMessage(NoteaconsAction action, String title, String message) {
        // Here do your implementation of Action Message.
        // When the action is finished, you need to call action.finish(action.getActionId(), context).
        // void finish(int actionId, Context context);
        // Return true to NOT run the default implementation. 
        return true;
    }
```

####Custom Callback

To implement the custom callback, you have to override the `onExecuteCustomCallback`:

```
@Override
    public boolean onExecuteActionCustomCallback(NoteaconsAction action, String functionName, String params) {
        // Do what you want here.
        // When the action is finished, you need to call action.finish(action.getActionId(), context).
        // void finish(int actionId, Context context);
        // Return true to NOT run the default implementation.
        return true;
    }
```












