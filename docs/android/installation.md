#Installation

Follow these steps to integrate the Noteacons SDK within an existing project:

Add the JitPack repository in your root _build.gradle_ at the end of repositories:

```
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```

Add the dependency in your app _build.gradle_:

```
dependencies {
    compile 'org.bitbucket.deverywareiberia:sdk-android:v1.2.0'
}
```

This version works with:

* **Google Play Services 9.8.0**
* **[Android Beacon Library 2.9.1](http://altbeacon.github.io/android-beacon-library/)**
* **Android Support Library v4 25.0.0**
