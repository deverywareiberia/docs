#Noteacons SDK

Welcome to Noteacons! 

Simple, effective and potent proximity campaigns. 

If you want to edit your campaigns, go to our [dashboard](http://panel.noteacons.com/) and use our [3 simple steps to launch guide](http://www.noteacons.com/support/) to get started.




